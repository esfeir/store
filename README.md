#Retail Store
##Intro
This is a retail store, with the following discounts apply:

1- If the user is an employee of the store, he gets a 30% discount.

2- If the user is an affiliate of the store, he gets a 10% discount.

3- If the user has been a customer for over 2 years, he gets a 5% discount.

4- For every $100 on the bill, there would be a $ 5 discount (e.g. for $ 990, you get $ 45
as a discount).

5- The percentage based discounts do not apply on groceries.

6- A user can get only one of the percentage based discounts on a bill.

## Setup
To Setup the project please do the following:

1- Clone the project from the following repository.
```bash
	git clone https://bitbucket.org/esfeir/store
```

2- Open the command line and access the root directory of the project

3- Run the following command
```bash
   mvn install
```

## Unit Test
To run the unit tests run the following command

1- You can run the unit tests by running the following command at command prompt:
```bash
   mvn clean test
```

2- If you don’t want to clean the build before running the unit tests, you have to use the command:
```bash
   mvn test
```
