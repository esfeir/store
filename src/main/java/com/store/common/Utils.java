package com.store.common;

import java.util.Calendar;
import java.util.Date;

public class Utils {
	public static boolean isCreatedMoreThan2Years(Date created) {
		Date currentDate = new Date();
		return getYearsDifference(created,currentDate) >= 2;
	}
	
	public static int getYearsDifference(Date first, Date second) {
	    Calendar a = getCalendar(first);
	    Calendar b = getCalendar(second);
	    
	    int diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR);
	    if (a.get(Calendar.DAY_OF_YEAR) > b.get(Calendar.DAY_OF_YEAR)) {
	        diff--;
	    }
	    
	    return diff;
	}
	
	public static Calendar getCalendar(Date date) {
	    Calendar cal = Calendar.getInstance();
	    cal.setTime(date);
	    return cal;
	}
}
