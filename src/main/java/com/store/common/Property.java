package com.store.common;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class Property {
	private static String employeePercentage;
	private static String affiliatePercentage;
	private static String customerPercentage;
	private static String everyAmountOnCart;
	private static String discountOnCart;

	public Property() {
		try {
			Properties properties = new Properties();
			InputStream input = new FileInputStream("config.properties");
			properties.load(input);

			employeePercentage = properties.getProperty("employee.percentage");
			affiliatePercentage = properties.getProperty("affiliate.percentage");
			customerPercentage = properties.getProperty("customer.percentage");
			everyAmountOnCart = properties.getProperty("cart.everyAmount");
			discountOnCart = properties.getProperty("cart.discount");
		} catch (Exception e) {

		}
	}

	public static String getEmployeePercentage() {
		return employeePercentage;
	}

	public static void setEmployeePercentage(String employeePercentage) {
		Property.employeePercentage = employeePercentage;
	}

	public static String getAffiliatePercentage() {
		return affiliatePercentage;
	}

	public static void setAffiliatePercentage(String affiliatePercentage) {
		Property.affiliatePercentage = affiliatePercentage;
	}

	public static String getCustomerPercentage() {
		return customerPercentage;
	}

	public static void setCustomerPercentage(String customerPercentage) {
		Property.customerPercentage = customerPercentage;
	}

	public static String getEveryAmountOnCart() {
		return everyAmountOnCart;
	}

	public static void setEveryAmountOnCart(String everyAmountOnCart) {
		Property.everyAmountOnCart = everyAmountOnCart;
	}

	public static String getDiscountOnCart() {
		return discountOnCart;
	}

	public static void setDiscountOnCart(String discountOnCart) {
		Property.discountOnCart = discountOnCart;
	}
}
