package com.store.models.user;

import java.util.Date;

import com.store.services.user.UserService;

public class User {
	private String firstName;
	private String lastName;
	private UserType type;
	private Date created;
	private double percentageDiscount;
	
	public User(String firstName, String lastName, UserType type, Date created){
		this.firstName = firstName;
		this.lastName = lastName;
		this.type = type;
		this.created = created;
		this.percentageDiscount = UserService.getUserPercentageDiscount(type, created);
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public UserType getType() {
		return type;
	}
	public void setType(UserType type) {
		this.type = type;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public double getDiscountPercentage() {
		return percentageDiscount;
	}
	public void setDiscountPercentage(double percentageDiscount) {
		this.percentageDiscount = percentageDiscount;
	}
}
