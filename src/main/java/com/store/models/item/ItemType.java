package com.store.models.item;

public enum ItemType {
	GROCERY,
	FOOD,
	BOOK,
	DEVICE,
	ACCESSORY,
	OTHER
}
