package com.store.models.item;

import com.store.services.item.ItemService;

public class Item {
	private String name;
	private Integer quantity;
	private ItemType type;
	private Double price;
	private Double total;
	private boolean isDiscountable = false;
	
	public Item(String name, Integer quantity, ItemType type, Double price){
		this.name = name;
		this.quantity = quantity;
		this.type = type;
		this.price = price;
		this.total = quantity * price;
		this.isDiscountable = !ItemService.isGrocery(type);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public ItemType getType() {
		return type;
	}
	public void setType(ItemType type) {
		this.type = type;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public boolean isDiscountable() {
		return isDiscountable;
	}
	public void setDiscountable(boolean isDiscountable) {
		this.isDiscountable = isDiscountable;
	}
}
