package com.store.services.cart;

import java.util.ArrayList;

import com.store.common.Property;
import com.store.models.cart.Cart;
import com.store.models.item.Item;

public class CartService {
	public static void updateCart(Cart cart){
		ArrayList<Item> items = cart.getItems();
		double total = 0; //original total 
		double totalWithDiscount = 0; //used to apply discount
		double totalWithoutDiscount = 0; //discount is not applicable on this total
		
		for(Item item: items){
			total += item.getTotal();
			
			if(item.isDiscountable()){
				totalWithDiscount += item.getTotal();
			}else{
				totalWithoutDiscount += item.getTotal();
			}
		}
		
		totalWithDiscount = totalWithDiscount - (totalWithDiscount * cart.getDiscountPercentage());
		
		double grossTotal = totalWithDiscount + totalWithoutDiscount;
		int every = (int) grossTotal / Integer.parseInt(Property.getEveryAmountOnCart());
		grossTotal = grossTotal - (every * Integer.parseInt(Property.getDiscountOnCart()));
		
		cart.setTotal(total);
		cart.setDiscountValue(total - grossTotal);
		cart.setGrossTotal(grossTotal);
	}
}
