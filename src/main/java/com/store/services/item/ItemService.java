package com.store.services.item;

import com.store.models.item.ItemType;

public class ItemService {
	public static boolean isGrocery(ItemType type){
		return type == ItemType.GROCERY;
	}
	
	public static boolean isFood(ItemType type){
		return type == ItemType.FOOD;
	}
	
	public static boolean isBook(ItemType type){
		return type == ItemType.BOOK;
	}
	
	public static boolean isDevice(ItemType type){
		return type == ItemType.DEVICE;
	}
	
	public static boolean isAccessory(ItemType type){
		return type == ItemType.ACCESSORY;
	}
}
