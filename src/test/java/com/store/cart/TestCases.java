package com.store.cart;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.store.common.Property;
import com.store.models.cart.Cart;
import com.store.models.item.Item;
import com.store.models.item.ItemType;
import com.store.models.user.User;
import com.store.models.user.UserType;
import com.store.services.cart.CartService;
import com.store.services.item.ItemService;
import com.store.services.user.UserService;

@TestMethodOrder(OrderAnnotation.class)
public class TestCases {
	@BeforeAll
	public static void start() {
		//execute to get properties from config file
		Property prop = new Property();
	}
	
	@Test
	@Order(1)
	public void checkDefinedProperties(){
		assertEquals(Property.getEmployeePercentage(), "0.3");
		assertEquals(Property.getAffiliatePercentage(), "0.1");
		assertEquals(Property.getCustomerPercentage(), "0.05");
		assertEquals(Property.getEveryAmountOnCart(), "100");
		assertEquals(Property.getDiscountOnCart(), "5");
	}
	
	@Test
	@Order(2)
	public void checkUsersType(){
		Date currentDate = new Date();
		User employeeUser = new User("Elias", "Employee", UserType.EMPLOYEE, currentDate);
		User affiliateUser = new User("Elias", "Affiliate", UserType.AFFIlIATE, currentDate);
		User customerUser = new User("Elias", "Customer", UserType.CUSTOMER, currentDate);
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, -2);
		Date past2Years = cal.getTime();
		User customerUserMoreThan2Years = new User("Elias", "Customer with 2 years", UserType.CUSTOMER, past2Years);
		
		assertTrue(UserService.isEmplpoyee(employeeUser.getType()));
		assertTrue(UserService.isAffiliate(affiliateUser.getType()));
		assertTrue(UserService.isCustomer(customerUser.getType()));
		assertTrue(UserService.isCustomerMoreThan2Years(customerUserMoreThan2Years.getType(), customerUserMoreThan2Years.getCreated()));
	}
	
	@Test
	@Order(3)
	public void checkItemsType(){
		Item iPhone = new Item("Iphone", 1 , ItemType.DEVICE, 750.0);
		Item grocery = new Item("Grocery", 2, ItemType.GROCERY, 100.0);
		
		assertTrue(ItemService.isDevice(iPhone.getType()));
		assertTrue(ItemService.isGrocery(grocery.getType()));
	}
	
	@Test
	@Order(4)
	public void checkUserDiscounts(){
		Date currentDate = new Date();
		User employeeUser  = new User("Elias", "Employee", UserType.EMPLOYEE, currentDate);
		User affiliateUser = new User("Elias", "Affiliate", UserType.AFFIlIATE, currentDate);
		User customerUser = new User("Elias", "Customer", UserType.CUSTOMER, currentDate);
		User otherUser = new User("Elias", "Other", UserType.OTHER, currentDate);
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, -2);
		Date past2Years = cal.getTime();
		User customerUserMoreThan2Years = new User("Elias", "Customer with 2 years", UserType.CUSTOMER, past2Years);
		User employeeUserMoreThan2Years  = new User("Elias", "Employee", UserType.EMPLOYEE, past2Years);
		
		assertEquals(employeeUser.getDiscountPercentage(), 0.3);
		assertEquals(affiliateUser.getDiscountPercentage(), 0.1);
		assertEquals(customerUser.getDiscountPercentage(), 0);
		assertEquals(otherUser.getDiscountPercentage(), 0);
		assertEquals(customerUserMoreThan2Years.getDiscountPercentage(), 0.05);
		assertEquals(employeeUserMoreThan2Years.getDiscountPercentage(), 0.3);
	}
	

	@Test
	@Order(5)
	public void checkCartForEmployee(){
		Date currentDate = new Date();
		User employeeUser  = new User("Elias", "Employee", UserType.EMPLOYEE, currentDate);
		Item charger = new Item("Charger", 1, ItemType.ACCESSORY, 40.0);
		Item iPhone = new Item("Iphone", 1 , ItemType.DEVICE, 750.0);
		Item grocery = new Item("Grocery", 2, ItemType.GROCERY, 10.0);
		Item book = new Item("Book", 2, ItemType.BOOK, 100.0);
		
		ArrayList<Item> items = new ArrayList<Item>();
		items.add(charger);
		items.add(iPhone);
		items.add(grocery);
		items.add(book);
		
		Cart cart = new Cart(employeeUser, items);
		CartService.updateCart(cart);
		
		assertEquals(cart.getDiscountPercentage(), 0.3);
		assertEquals(cart.getGrossTotal(), 678.0);
	}
	
	@Test
	@Order(6)
	public void checkCartForAffiliate(){
		Date currentDate = new Date();
		User employeeUser  = new User("Elias", "Affiliate", UserType.AFFIlIATE, currentDate);
		Item charger = new Item("Charger", 1, ItemType.ACCESSORY, 40.0);
		Item iPhone = new Item("Iphone", 1 , ItemType.DEVICE, 750.0);
		Item grocery = new Item("Grocery", 2, ItemType.GROCERY, 10.0);
		Item book = new Item("Book", 2, ItemType.BOOK, 100.0);
		
		ArrayList<Item> items = new ArrayList<Item>();
		items.add(charger);
		items.add(iPhone);
		items.add(grocery);
		items.add(book);
		
		Cart cart = new Cart(employeeUser, items);
		CartService.updateCart(cart);
		
		assertEquals(cart.getDiscountPercentage(), 0.1);
		assertEquals(cart.getGrossTotal(), 866.0);
	}
	
	@Test
	@Order(7)
	public void checkCartForNewCustomer(){
		Date currentDate = new Date();
		User employeeUser  = new User("Elias", "Customer", UserType.CUSTOMER, currentDate);
		Item charger = new Item("Charger", 1, ItemType.ACCESSORY, 40.0);
		Item iPhone = new Item("Iphone", 1 , ItemType.DEVICE, 750.0);
		Item grocery = new Item("Grocery", 2, ItemType.GROCERY, 10.0);
		Item book = new Item("Book", 2, ItemType.BOOK, 100.0);
		
		ArrayList<Item> items = new ArrayList<Item>();
		items.add(charger);
		items.add(iPhone);
		items.add(grocery);
		items.add(book);
		
		Cart cart = new Cart(employeeUser, items);
		CartService.updateCart(cart);
		
		assertEquals(cart.getDiscountPercentage(), 0.0);
		assertEquals(cart.getGrossTotal(), 960.0);
	}
	
	@Test
	@Order(8)
	public void checkCartForOldCustomer(){
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, -3);
		Date past3Years = cal.getTime();
		
		User employeeUser  = new User("Elias", "Old Customer", UserType.CUSTOMER, past3Years);
		Item charger = new Item("Charger", 1, ItemType.ACCESSORY, 40.0);
		Item iPhone = new Item("Iphone", 1 , ItemType.DEVICE, 750.0);
		Item grocery = new Item("Grocery", 2, ItemType.GROCERY, 10.0);
		Item book = new Item("Book", 2, ItemType.BOOK, 100.0);
		
		ArrayList<Item> items = new ArrayList<Item>();
		items.add(charger);
		items.add(iPhone);
		items.add(grocery);
		items.add(book);
		
		Cart cart = new Cart(employeeUser, items);
		CartService.updateCart(cart);
		
		assertEquals(cart.getDiscountPercentage(), 0.05);
		assertEquals(cart.getGrossTotal(), 915.5);
	}
}
